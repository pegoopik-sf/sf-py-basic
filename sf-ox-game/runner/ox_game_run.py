import string
import random


HINT_DICT = {'_________': (0, 0), '_O__X____': (-1, 0), '____XO___': (0, 1), '____X__O_': (1, 0),
             '___OX____': (0, -1), '_O_XXO___': (-1, 1), '_X__XO_O_': (1, 1), '___OXX_O_': (1, -1),
             '_O_OX__X_': (-1, -1), '___XXO_O_': (-1, -1), '_O__XO_X_': (-1, 1), '_O_OXX___': (1, 1),
             '_X_OX__O_': (1, -1), 'O___X____': (0, -1), '__O_X____': (-1, 0), '____X___O': (0, 1),
             '____X_O__': (1, 0), 'OX__X__O_': (1, -1), '__OOXX___': (-1, -1), '_O__X__XO': (-1, 1),
             '___XXOO__': (1, 1), '_O__X_OX_': (1, 1), '___OXX__O': (1, -1), '_XO_X__O_': (-1, -1),
             'O__XXO___': (-1, 1), '____X____': (-1, 1), 'X________': (0, 0), '__X______': (0, 0),
             '________X': (0, 0), '______X__': (0, 0), '_X_______': (0, 0), '_____X___': (0, 0),
             '_______X_': (0, 0), '___X_____': (0, 0)}

class Point2D:
    x: int
    y: int

    def __init__(self, _x, _y):
        self.x = _x
        self.y = _y

    def __str__(self):
        return '(' + str(self.x) + ', ' + str(self.y) + ')'


class SfCell:
    point: Point2D
    str_val: string = '_'
    int_val: int = 0

    def __init__(self, point: Point2D, str_val='_', int_val=0):
        self.point = point
        self.str_val = str_val

    def set_str_val(self, str_val: string):
        if self.str_val != '_':
            raise Exception("Illegal FsCell state self.str_val != '_' : " + self.str_val)
        self.str_val = str_val
        if str_val == 'O':
            self.int_val = -1
        elif str_val == 'X':
            self.int_val = 1
        else:
            raise Exception("Illegal argument str_val == " + str_val)

class SfGame:
    game_group_list = [].copy()
    game_cell_list = [].copy()

    def __str__(self):
        result = '  |0|1|2|\n'
        result += '|0|' + self.game_cell_list[0].str_val + '|' + self.game_cell_list[1].str_val + '|' + self.game_cell_list[2].str_val + '|\n'
        result += '|1|' + self.game_cell_list[3].str_val + '|' + self.game_cell_list[4].str_val + '|' + self.game_cell_list[5].str_val + '|\n'
        result += '|2|' + self.game_cell_list[6].str_val + '|' + self.game_cell_list[7].str_val + '|' + self.game_cell_list[8].str_val + '|'
        return result

    def set_cell_str(self, str_val: string, x: int, y: int):
        self.game_cell_list[y * 3 + x].set_str_val(str_val)

    def get_geme_str(self):
        result = ''
        for game_cell in self.game_cell_list:
            result += game_cell.str_val
        return result


class SfGroup:
    cell_list = [] # список ячеек в группе
    is_accept: () # проверяем, подходит там ячейка или нет

    def __int__(self):
        self.cell_list = []


def get_hint(game: SfGame, level: int):
    # TODO if level > 1: # вернуть после исправления ошибки, пока выключил
    if level == level + 1:
        for _group in game.game_group_list:
            sum_int_value = 0
            last_zero_cell = None
            for _cell in _group.cell_list:
                sum_int_value += _cell.int_val
                if _cell.int_val == 0:
                    last_zero_cell = _cell
            if sum_int_value in (2, -2):
                # TODO перепутаны где-то x и y
                return last_zero_cell.point.x, last_zero_cell.point.y
    if level > 2:
        _key = game.get_geme_str()
        if HINT_DICT.get(_key):
            return HINT_DICT[_key]

    zero_cell_list = []
    for _cell in game.game_cell_list:
        if _cell.int_val == 0:
            zero_cell_list.append(_cell)
    random_index = random.randrange(len(zero_cell_list))
    random_zero_cell_point = zero_cell_list[random_index].point
    # TODO перепутаны где-то x и y return random_zero_cell_point.x, random_zero_cell_point.y
    return random_zero_cell_point.y, random_zero_cell_point.x


def step(game: SfGame, is_computer: bool, level: int, user_name: string, user_char: string):
    print(game)
    if is_computer:
        hint_x, hint_y = get_hint(game, level);
        print(user_name + '(' + user_char + ') ходит:', (hint_x + 1, hint_y + 1))
        sf_game.set_cell_str(user_char, hint_x + 1, hint_y + 1)
    else:
        input_list = input(user_name + '(' + user_char + '), введите координаты > ').split(' ')
        # if input_list[0] == 'hint':
        #     print('Здесь будет выведена подсказка')
        #     return False
        input_x, input_y = int(input_list[0]), int(input_list[1])
        sf_game.set_cell_str(user_char, input_x, input_y)
    return False


# TODO унести из глобальных переменных
group_list = [];

for i in range(-1, 2):
    # Создаём строки
    group_line: SfGroup = SfGroup()
    group_line.is_accept = lambda point: i == point.x
    # TODO [].copy - костыль, но в глобальных переменных он иначе "разные" списки считает одним
    # TODO спросить у вендора как это лучше исправить
    group_line.cell_list = [].copy()
    group_list.append(group_line)
    # Создаём столбцы
    group_column: SfGroup = SfGroup()
    group_column.cell_list = [].copy()
    group_column.is_accept = lambda point: i == point.y
    group_list.append(group_column)

# прямая диагональ
group_cross_1: SfGroup = SfGroup()
group_cross_1.cell_list = [].copy()
group_cross_1.is_accept = lambda point: point.x == point.y
group_list.append(group_cross_1)
# обратная диагональ
group_cross_2: SfGroup = SfGroup()
group_cross_2.cell_list = [].copy()
group_cross_2.is_accept = lambda point: point.x == -point.y
group_list.append(group_cross_2)

sf_game: SfGame = SfGame()
sf_game.game_group_list = group_list

for x in range(-1, 2):
    for y in range(-1, 2):
        point: Point2D = Point2D(x, y)
        cell: SfCell = SfCell(point)
        sf_game.game_cell_list.append(cell)
        for group in group_list:
            if group.is_accept(cell.point):
                group.cell_list.append(cell)


print('Добро пожаловать в крестики/нолики!')
print()
print('Кто будет играть? Выберите один из вариантов:')
print('  0 - Человек(X) vs Человек(O)')
print('  1 - Человек(X) vs Компьютер(O)')
print('  2 - Компьютер(X) vs Человек(O)')
print('  3 - Компьютер(X) vs Компьютер(O)')
input_case = int(input('> '))


if input_case in (0, 1):
    user1_level = 10
    user1_name = input('Введите имя пользователя (X) > ')
    user1_char = 'X'
    user1_is_comp = False
if input_case in (2, 3):
    user1_level = int(input('Сложность компьютера 1 (X) от 1 до 3  > '))
    user1_name = 'comp1_level_' + str(user1_level)
    user1_char = 'X'
    user1_is_comp = True
if input_case in (0, 2):
    user2_level = 10
    user2_name = input('Введите имя пользователя (O) > ')
    user2_char = 'O'
    user2_is_comp = False
if input_case in (1, 3):
    user2_level = int(input('Сложность компьютера 1 (O) от 1 до 3  > '))
    user2_name = 'comp1_level_' + str(user2_level)
    user2_char = 'O'
    user2_is_comp = True

print('Играет ' + user1_name + '(X) против ' + user2_name + '(O)')

step_count = 0
is_finish = False

while True:
    if step_count % 2 == 0:
        is_finish = step(sf_game, user1_is_comp, user1_level, user1_name, user1_char)
    else:
        is_finish = step(sf_game, user2_is_comp, user2_level, user2_name, user2_char)
    step_count += 1
    if is_finish:
        break
