import string

incoming_cases = [].copy()
incoming_cases.append('_________ 0 0')
incoming_cases.append('_O__X____ -1 0')
incoming_cases.append('_O_XXO___ -1 1')
incoming_cases.append('O___X____ 0 -1')
incoming_cases.append('OX__X__O_ 1 -1')
incoming_cases.append('____X____ -1 -1')
incoming_cases.append('X________ 0 0')
incoming_cases.append('_X_______ 0 0')


class PointHint:
    x: int
    y: int
    chr: string

    def __str__(self):
        return 'PointHint: ' + str(self.x) + '/' + str(self.y) + '/' + chr

class HintContainer:
    x: int
    y: int
    n = None
    s_key = 's_key'
    calc_s_key = 'qweqweqwe'
    # TODO delete
    point_list = [].copy()

    def __str__(self):
        return str(self.x) + str(self.y) + ':' + str(self.n) + '|' + self.s_key + '| |' + self.calc_s_key + '|'

def n_to_x(n: int):
    return (n % 3) - 1


def n_to_y(n: int):
    return (n // 3) - 1


def xy_to_n(x: int, y: int):
    return (y + 1) * 3 + x + 1


def rotate_90(x: int, y: int):
    return y, -x


def mirror_x(x: int, y: int):
    return -x, y


def mirror_y(x: int, y: int):
    return x, -y


def migrate_xy(x: int, y: int, need_mirror_x: bool, need_mirror_y: bool, rotate_90_cnt: int):
    new_x, new_y = x, y
    if need_mirror_x:
        new_x, new_y = mirror_x(new_x, new_y)
    if need_mirror_y:
        new_x, new_y = mirror_y(new_x, new_y)
    if rotate_90_cnt > 0:
        for i in range(rotate_90_cnt):
            new_x, new_y = rotate_90(new_x, new_y)
    return new_x, new_y


''' # check migrate_xy
xx, yy = -1, 1
print(xx, yy)
xx, yy = migrate_xy(xx, yy, False, False, 3)
print(xx, yy)
'''


container_list = [].copy()
container_list_created = [].copy()

for inc_case in incoming_cases:
    lexems = inc_case.split(' ')
    hint_x, hint_y = int(lexems[1]), int(lexems[2])
    hint_container: HintContainer = HintContainer()
    hint_container.x = hint_x
    hint_container.y = hint_y
    hint_container.n = xy_to_n(hint_x, hint_y)
    curr_key = lexems[0]
    hint_container.s_key = curr_key
    hint_container.point_list = [].copy()
    chr_index = 0
    for chr in curr_key:
        cur_x, cur_y = n_to_x(chr_index), n_to_y(chr_index)
        print(chr_index, chr, cur_x, cur_y) # , xy_to_n(cur_x, cur_y))
        chr_index += 1
        curr_hint_point: PointHint = PointHint()
        curr_hint_point.chr = chr
        curr_hint_point.x, curr_hint_point.y = cur_x, cur_y
        hint_container.point_list.append(curr_hint_point)
    # TODO уменьшить вложимасть
    for need_mirror_x in (False, True):
        for need_mirror_y in (False, True):
            for cnt_90 in range(4):
                # print('mirror', need_mirror_x, need_mirror_y, cnt_90)
                hint_container_created: HintContainer = HintContainer()
                hint_container_created.x, hint_container_created.y = migrate_xy(
                    hint_container.x, hint_container.y, need_mirror_x, need_mirror_y, cnt_90)
                hint_container_created.n = xy_to_n(hint_container_created.x, hint_container_created.y)
                hint_container_created.point_list = [].copy()
                hint_container_created.s_key = hint_container.s_key
                for hit_point in hint_container.point_list:
                    hint_container_created.point_list.append(hit_point)

# string = 'tonaisuu' position = 6 new_character = 'r' temp = list(string) temp[position] = new_character string = "".join(temp) print(string)
# Источник: https://tonais.ru/string/zamena-simvola-v-stroke-po-indeksu-v-python

                calc_s_key_list = list('qweqweqwe').copy()

                for calc_char_index in range(9):
                    calc_x, calc_y = migrate_xy(
                        n_to_x(calc_char_index), n_to_y(calc_char_index), need_mirror_x, need_mirror_y, cnt_90)
                    calc_s_key_list[calc_char_index] = hint_container_created.s_key[xy_to_n(calc_x, calc_y)]
                    # print(calc_s_key_list)
                hint_container_created.calc_s_key = "".join(calc_s_key_list)
                    # hint_container_created.calc_s_key[calc_char_index] = \
                    #     hint_container_created.s_key[xy_to_n(calc_x, calc_y)]
                # hint_container_created: HintContainer = hint_container.copy()
                # print('hint_container_created', hit_point)
                print('mirror', need_mirror_x, need_mirror_y, cnt_90,
                      'curr_container', hint_container, 'calc_container', hint_container_created)
                container_list_created.append(hint_container_created)
                #hint_container_created.x, hint_container_created.y =




    container_list.append(hint_container)
    print(hint_container)

    hint_dict = {}
    for hint_container_for_dict in container_list_created:
        hint_dict[hint_container_for_dict.calc_s_key] = hint_container_for_dict.x, hint_container_for_dict.y


print(hint_dict)

# В итоге всего 34 подсказки сгенерировалось:

calс_hint_dict = {'_________': (0, 0), '_O__X____': (-1, 0), '____XO___': (0, 1), '____X__O_': (1, 0),
                  '___OX____': (0, -1), '_O_XXO___': (-1, 1), '_X__XO_O_': (1, 1), '___OXX_O_': (1, -1),
                  '_O_OX__X_': (-1, -1), '___XXO_O_': (-1, -1), '_O__XO_X_': (-1, 1), '_O_OXX___': (1, 1),
                  '_X_OX__O_': (1, -1), 'O___X____': (0, -1), '__O_X____': (-1, 0), '____X___O': (0, 1),
                  '____X_O__': (1, 0), 'OX__X__O_': (1, -1), '__OOXX___': (-1, -1), '_O__X__XO': (-1, 1),
                  '___XXOO__': (1, 1), '_O__X_OX_': (1, 1), '___OXX__O': (1, -1), '_XO_X__O_': (-1, -1),
                  'O__XXO___': (-1, 1), '____X____': (-1, 1), 'X________': (0, 0), '__X______': (0, 0),
                  '________X': (0, 0), '______X__': (0, 0), '_X_______': (0, 0), '_____X___': (0, 0),
                  '_______X_': (0, 0), '___X_____': (0, 0)}
